<?php
namespace classes\helpers;

class Converter {

	/**
	 * Calculate percentage of value
	 * @param int $value
	 * @param int $promoPercent
	 * @return int
	 */
	public static function calcPromo($value, $promoPercent) {
		$ratio = (100 - $promoPercent) / 100;

		//TODO Check rounding algorithm
		return (int)round($value * $ratio);
	}

	public static function integerPriceToText($price, $decimals) {
		return number_format($price / pow(10, $decimals), $decimals);
	}

	/**
	 * Alternative uniqid()
	 * @param string $prefix
	 * @return string
	 */
	public static function uniqueId($prefix = '') {
		return strtoupper($prefix . sprintf('_%04x-%04x-%04x',
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
			));
		//return strtoupper(md5(openssl_random_pseudo_bytes(32)));
	}
}
