<?php

namespace classes\helpers;

class Validator {

	#region Type constant

	const TYPE_STRING = 'string';

	const TYPE_INT = 'integer';

	const TYPE_NULL = 'NULL';

	const TYPE_ARRAY = 'array';

	const TYPE_FLOAT = 'double';

	const TYPE_BOOL = 'boolean';

	const TYPE_OBJECT = 'object';

	const TYPE_RESOURCE = 'resource';

	#endregion

	//For increase performance. Array keys are cached
	private static $all_types = [
		self::TYPE_STRING => null,
		self::TYPE_INT => null,
		self::TYPE_NULL => null,
		self::TYPE_ARRAY => null,
		self::TYPE_FLOAT => null,
		self::TYPE_BOOL => null,
		self::TYPE_OBJECT => null,
		self::TYPE_RESOURCE => null,
	];

	private static $errors = [];

	/**
	 * Check variable type
	 *
	 * @param mixed $attribute
	 * @param mixed $type
	 * @return bool
	 */
	public static function check($attribute, $type) {
		if (!self::checkType($type)) {
			return false;
		}

		$real_type = gettype($attribute);
		$checkEmpty = false;

		if ($type != self::TYPE_INT) {
			$checkEmpty = empty($attribute);
		}

		if ($checkEmpty || !is_string($type) || $real_type != $type) {
			$dump = print_r($attribute, true);
			self::addError("Wrong attribute ({$real_type} != {$type}) {$dump}");

			return false;
		}

		return true;
	}

	/**
	 * Add error to stack
	 * @param $errorText
	 */
	public static function addError($errorText) {
		self::$errors[] = $errorText;
	}

	/**
	 * Clear stack of errors
	 */
	public static function clearError() {
		self::$errors = [];
	}

	/**
	 * Return stack of errors
	 * @return array
	 */
	public static function getError() {
		return self::$errors;
	}

	/**
	 * Convert error stack to string for printing
	 * @param string $separator
	 * @return string
	 */
	public static function getErrorText($separator = "\n") {
		return implode($separator, self::$errors);
	}

	/**
	 * Check declared of type
	 * @param $type
	 * @return bool
	 */
	private static function checkType($type) {
		if (empty($type) || !is_string($type) || !array_key_exists($type, self::$all_types)) {
			$real_type = gettype($type);
			$dump = print_r($type, true);
			self::addError("Wrong type ({$real_type}) {$dump}");

			return false;
		}

		return true;
	}
}
