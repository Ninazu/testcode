<?php

namespace classes\helpers;

class Regexp {

	private $openTag;

	private $closeTag;

	private $delimiter;

	private $replaceTo = [];

	private $pattern;

	public function __construct($openTag = '{', $closeTag = '}', $delimiter = '|') {
		$this->openTag = $openTag;
		$this->closeTag = $closeTag;
		$this->delimiter = $delimiter;
		$safeOpenTag = preg_quote($this->openTag, '\\');
		$safeCloseTag = preg_quote($this->closeTag, '\\');
		$this->pattern = "/{$safeOpenTag}((?>(?>(?!{$safeOpenTag}|{$safeCloseTag}).)+|(?R))*){$safeCloseTag}/sxi";
	}

	/**
	 * Generate random text
	 * @param string $text
	 * @return string
	 */
	public function generateRandomText($text) {
		$this->recursiveSearchTags($text);
		$this->replace($text);

		return $text;
	}

	/**
	 * Replace all find part
	 * @param string $text
	 */
	private function replace(&$text) {
		foreach ($this->replaceTo as $search => $replace) {
			$text = str_replace("{$this->openTag}{$search}{$this->closeTag}", $replace, $text);
		}
	}

	/**
	 * Explode variant and replace from random
	 * @param string $text
	 */
	private function explodeAndRandomReplace($text) {
		$variant = explode($this->delimiter, $text);
		$this->replace($text);
		$this->replaceTo[$text] = "<span>" . $variant[rand(0, count($variant) - 1)] . "</span>";
	}

	/**
	 * Recursive search regexp
	 * @param string $text
	 * @return bool
	 */
	private function recursiveSearchTags($text) {
		$results = [];

		if (preg_match_all($this->pattern, $text, $results)) {
			foreach ($results[1] as $result) {
				if ($this->recursiveSearchTags($result)) {
					$this->replace($result);
				}

				$this->explodeAndRandomReplace($result);
			}

			return true;
		} else {
			$this->explodeAndRandomReplace($text);

			return false;
		}
	}
}
