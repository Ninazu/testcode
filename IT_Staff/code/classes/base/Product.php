<?php
namespace classes\base;

use classes\helpers\Converter;
use classes\helpers\Validator;

/**
 * @property \stdClass $promo
 */
class Product {

	private $name;

	private $price = 0;

	private $promo_price = 0;

	public $id; //Default product price

	public $promo; //Default promo product price

	/**
	 * Product constructor.
	 * @param string $name - Product name
	 * @param int $price - Price always stored in Integer
	 * @throws \Exception
	 */
	public function __construct($name, $price = null) {
		if (!Validator::check($name, Validator::TYPE_STRING)) {
			throw new \Exception(Validator::getErrorText());
		}

		$this->name = $name;

		if (!is_null($price)) {
			if (!Validator::check($price, Validator::TYPE_INT)) {
				throw new \Exception(Validator::getErrorText());
			}

			$this->price = $price;
			$this->promo_price = $price;
		}
	}

	/**
	 * Return product name
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Return product price as Integer
	 * @param bool $promo
	 * @return int
	 */
	public function getPrice($promo = false) {
		if ($promo) {
			return $this->promo_price;
		}

		return $this->price;
	}

	/**
	 * Set product price
	 * @param int $integer
	 * @param int $fraction
	 * @param bool $promo
	 * @return int current price
	 * @throws \Exception
	 */
	public function setPrice($integer, $fraction = 0, $promo = false) {
		if (!Validator::check($integer, Validator::TYPE_INT)) {
			throw new \Exception(Validator::getErrorText());
		}

		if (!Validator::check($fraction, Validator::TYPE_INT)) {
			throw new \Exception(Validator::getErrorText());
		}

		//Fast convert to string
		$fraction = $fraction . '';
		$decimals = Market::getDecimals();

		if (strlen($fraction) > $decimals) {
			//TODO Exception instead cutting
			//$fraction = substr($fraction, 0, $this->decimals);
			throw new \Exception('Fraction is not formatted');
		}

		//Join to integer
		$price = $integer * pow(10, $decimals) + (int)str_pad($fraction, $decimals, '0', STR_PAD_RIGHT);

		if (!$promo) {
			$this->price = $price;
		}

		$this->promo_price = $price;

		return $price;
	}

	/**
	 * Sey product raw price as Integer
	 * @param int $value
	 * @param bool $promo
	 * @return int current price
	 * @throws \Exception
	 */
	public function setIntegerPrice($value, $promo = false) {
		if (!Validator::check($value, Validator::TYPE_INT)) {
			throw new \Exception('Wrong price format');
		}

		if (!$promo) {
			$this->price = $value;
		}

		$this->promo_price = $value;

		return $value;
	}

	/**
	 * Return product price as formatted text
	 * @param bool $promo
	 * @return string ;
	 */
	public function getPriceText($promo = false) {
		$price = ($promo) ? $this->promo_price : $this->price;

		return Converter::integerPriceToText($price, Market::getDecimals());
	}
}
