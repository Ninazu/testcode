<?php

namespace classes\base;

use classes\helpers\Converter;

class Cart {

	private $items;

	private $total;

	public $summaryDiscount;

	public function __construct() {
		$this->resetTotal();
	}

	/**
	 * Add product to cart
	 * @param Product $product
	 */
	public function add($product) {
		$newProduct = clone $product;
		$newProduct->id = Converter::uniqueId("Product");
		$this->items[] = $newProduct;
	}

	/**
	 * Mark item as apply promo
	 * @param Promo $promo
	 * @param int $itemIndex
	 */
	public function markAsPromo($promo, $itemIndex) {
		$this->items[$itemIndex]->promo = (object)[
			'id' => $promo->id,
			'class' => get_class($promo),
			'discount' => $promo->params[Promo::PARAMS_DISCOUNT],
		];
	}

	/**
	 * Return items in cart
	 * @return Product[]
	 */
	public function getItems() {
		return $this->items;
	}

	/**
	 * Calculate total sum
	 */
	public function calcTotal() {
		$this->resetTotal();

		foreach ($this->getItems() as $item) {
			$this->total['withPromo'] += $item->getPrice(true);
			$this->total['withoutPromo'] += $item->getPrice();
		}

		$this->total['withoutPromo'] = Converter::integerPriceToText($this->total['withoutPromo'], Market::getDecimals());

		if (isset($this->summaryDiscount)) {
			$discount = $this->summaryDiscount->discount;
			$withPriceAsInteger = Converter::calcPromo($this->total['withPromo'], $discount);
			$this->total['withSummaryPromo'] = Converter::integerPriceToText($withPriceAsInteger, Market::getDecimals());
		}

		return $this->total;
	}

	private function resetTotal() {
		$this->total = [
			'withPromo' => 0,
			'withoutPromo' => 0,
			'withSummaryPromo' => 0,
			'withoutSummaryPromo' => 0,
		];
	}
}
