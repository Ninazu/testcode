<?php
namespace classes\base;

/**
 * @property array $params
 * @property int
 */
abstract class Promo {

	const PARAMS_DISCOUNT = 'discount';

	const PARAMS_PRODUCT = 'product';

	const PARAMS_COUNT = 'count';

	const PARAMS_CONDITION = 'condition';

	const PARAMS_IGNORE = 'ignore';

	public $params;

	public $id;

	/**
	 * Promo constructor.
	 * @param array $params
	 */
	public function __construct($params = []) {
		$this->params = $params;
	}

	/**
	 * Apply promos to cart
	 * @param Cart $cart
	 * @return mixed
	 */
	abstract public function apply($cart);
}
