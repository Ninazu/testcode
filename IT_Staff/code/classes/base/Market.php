<?php
namespace classes\base;

use classes\helpers\Converter;

class Market {

	private $products = [];  //Sets the number of decimal points.

	private $promos = [];

	private static $decimals = 2;

	/**
	 * Configure products
	 * @param array $products
	 * @throws \Exception
	 */
	public function __construct(array $products = []) {
		foreach ($products as $product_name => $product_option) {
			if (is_numeric($product_option)) {
				$this->products[$product_name] = new Product($product_name, $product_option);
			} elseif ($product_option instanceof \classes\base\Product) {
				$newProduct = clone $product_option;
				$this->products[$newProduct->getName()] = $newProduct;
			} else {
				throw new \Exception('Wrong product');
			}
		}
	}

	/**
	 * Add promo to products
	 *
	 * @param string|Promo $promoClass Promo class name with namespace or Promo object
	 * @param array $params If promoClass is object, clone this object and his parameters will be automatically replaced
	 * @return Market
	 * @throws \Exception
	 */
	public function addPromo($promoClass, array $params = []) {
		if (is_string($promoClass)) {
			if (!class_exists($promoClass)) {
				throw new \Exception('Wrong promo class');
			}

			$newPromo = new $promoClass($params);
		} elseif ($promoClass instanceof \classes\base\Promo) {
			$newPromo = clone $promoClass;
			$newPromo->params = array_replace_recursive($promoClass->params, $params);
		} else {
			throw new \Exception('Wrong promo class');
		}

		$newPromo->id = Converter::uniqueId('Promo');
		$this->promos[] = $newPromo;

		return $newPromo->id;
	}

	/**
	 * Calculate prices for products and return recalculated cart
	 * @param array $products Array of product name or product object
	 * @return Cart
	 * @throws \Exception
	 */
	public function calculatePrice($products) {
		$cart = new Cart();

		foreach ($products as $product) {
			if (is_string($product)) {
				$productName = $product;
			} elseif ($product instanceof \classes\base\Product) {
				$productName = $product->getName();
			} else {
				throw new \Exception('Wrong product');
			}

			if (!isset($this->products[$productName])) {
				throw new \Exception('Product don\'t exist');
			}

			$productObject = $this->products[$productName];

			$cart->add($productObject);
		}

		/** @var Promo $promo */
		foreach ($this->promos as $promo) {
			$promo->apply($cart);
		}

		return $cart;
	}

	/**
	 * Global settings for decimals in price
	 * @return int
	 */
	public static final function getDecimals() {
		return self::$decimals;
	}
}
