<?php

namespace classes\promos;

use classes\base\Promo;

class CountPrice extends Promo {

	public function apply($cart) {
		$items = $cart->getItems();
		$condition = array_fill_keys($this->params[Promo::PARAMS_IGNORE], null);
		$max_count = $this->params[Promo::PARAMS_COUNT];

		$criteria = 0;

		foreach ($items as $index => $item) {
			//TODO If count only unused product
			if (isset($item->promo)) {
				continue;
			}

			$productName = $item->getName();

			if (!array_key_exists($productName, $condition)) {
				$criteria++;

				if ($criteria == $max_count) {
					////TODO Count only first Summary promo (Without check maximum discount)
					//if(!$cart->summaryDiscount){
					$cart->summaryDiscount = (object)[
						'id' => $this->id,
						'class' => get_class($this),
						'discount' => $this->params[Promo::PARAMS_DISCOUNT],
					];
					//}

					break;
				}
			}
		}
	}
}
