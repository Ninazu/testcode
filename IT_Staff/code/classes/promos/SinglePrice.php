<?php

namespace classes\promos;

use classes\base\Product;
use classes\base\Promo;
use classes\helpers\Converter;

class SinglePrice extends Promo {

	public function apply($cart) {
		$sourceProducts = array_fill_keys($this->params[Promo::PARAMS_PRODUCT], null);
		$condition = array_fill_keys($this->params[Promo::PARAMS_CONDITION], null);
		$items = $cart->getItems();

		foreach ($items as $index => $item) {
			if (count($item->promo)) {
				continue;
			}

			$productName = $item->getName();

			if (array_key_exists($productName, $sourceProducts)) {
				$sourceProducts[$productName][] = $index;

				if (!empty(array_filter($condition))) {
					break;
				}
			}

			if (array_key_exists($productName, $condition)) {
				$condition[$productName][] = $index;

				if (!empty(array_filter($sourceProducts))) {
					break;
				}
			}
		}

		$triggeredCondition = array_filter($condition);

		if (!empty($triggeredCondition)) {
			/** @var Product $item */
			$productInCondition = array_shift($triggeredCondition);
			$index = array_shift($productInCondition);
			$item = $items[$index];
			$promo_price = Converter::calcPromo($item->getPrice(), $this->params[Promo::PARAMS_DISCOUNT]);
			$item->setIntegerPrice($promo_price, true);
			$cart->markAsPromo($this, $index);
		}
	}
}
