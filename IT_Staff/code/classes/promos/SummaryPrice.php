<?php

namespace classes\promos;

use classes\base\Product;
use classes\base\Promo;
use classes\helpers\Converter;

class SummaryPrice extends Promo {

	/**
	 * @inheritdoc
	 */
	public function apply($cart) {
		$condition = array_fill_keys($this->params[Promo::PARAMS_CONDITION], null);
		$items = $cart->getItems();

		foreach ($items as $index => $item) {
			if (count($item->promo)) {
				continue;
			}

			$productName = $item->getName();

			if (array_key_exists($productName, $condition)) {
				$condition[$productName][] = $index;
			}
		}

		do {
			$applyTo = [];

			foreach ($condition as $name => &$indexes) {
				if (count($indexes) == 0) {
					break 2;
				}

				$index = array_shift($indexes);
				$applyTo[$index] = $items[$index];
			}

			/** @var Product $item */
			foreach ($applyTo as $index => $item) {
				$promo_price = Converter::calcPromo($item->getPrice(), $this->params[Promo::PARAMS_DISCOUNT]);
				$item->setIntegerPrice($promo_price, true);
				$cart->markAsPromo($this, $index);
			}
		} while (true);

		return;
	}
}
