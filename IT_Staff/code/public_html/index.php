<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

use classes\helpers\Regexp;
use classes\base\Market;
use classes\base\Product;
use classes\base\Promo;
use classes\promos\CountPrice;

#region AutoLoader

spl_autoload_register(function ($className) {
	$className = ltrim($className, '\\');
	$fileName = '';
	$class_dir = realpath(__DIR__ . '/../');

	if ($lastNsPos = strrpos($className, '\\')) {
		$namespace = substr($className, 0, $lastNsPos);
		$className = substr($className, $lastNsPos + 1);
		$fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
	}

	$fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
	$fileName = $class_dir . DIRECTORY_SEPARATOR . $fileName;

	require $fileName;
});

#endregion

#region Test1

#region Product price

//Storing price in Integer.

$A = new Product('A', 1233);
$B = new Product('B');
$B->setPrice(4345, 20);

$market = new Market([
	$A,                 //12.33
	$B,                 //4345.20
	'C' => 7890,        //78.90
	'D' => 123,         //1.23
	'E' => 0,           //0.00
	'F' => 100,         //1.00
	'G' => 70,          //0.70
	'H' => 1211,        //12.11
	'I' => 2457,        //24.57
	'J' => 1234,        //12.34
	'K' => 7899,        //78.99
	'L' => 100000,      //1000.00
	'M' => 99999,       //999.99
]);

#endregion

#region Promos

$countPrice = new CountPrice();
$countPrice->params = [
	Promo::PARAMS_IGNORE => ['A', 'C'],
	Promo::PARAMS_DISCOUNT => 5,
	Promo::PARAMS_COUNT => 3,
];

$market->addPromo('classes\promos\SummaryPrice', [
	Promo::PARAMS_DISCOUNT => 10,
	Promo::PARAMS_CONDITION => ['A', 'B'],
]);
$market->addPromo('classes\promos\SummaryPrice', [
	Promo::PARAMS_DISCOUNT => 5,
	Promo::PARAMS_CONDITION => ['D', 'E'],
]);
$market->addPromo('classes\promos\SummaryPrice', [
	Promo::PARAMS_DISCOUNT => 5,
	Promo::PARAMS_CONDITION => ['E', 'F', 'G'],
]);
$market->addPromo('classes\promos\SinglePrice', [
	Promo::PARAMS_DISCOUNT => 5,
	Promo::PARAMS_CONDITION => ['K', 'L', 'M'],
	Promo::PARAMS_PRODUCT => ['A']
]);
$market->addPromo($countPrice);
$market->addPromo($countPrice, [
	Promo::PARAMS_DISCOUNT => 10,
	Promo::PARAMS_COUNT => 4
]);
$market->addPromo('classes\promos\CountPrice', [
	Promo::PARAMS_DISCOUNT => 20,
	Promo::PARAMS_COUNT => 5,
	Promo::PARAMS_IGNORE => ['A', 'C']
]);

#endregion

#region Apply Promo

$cart = $market->calculatePrice([
	'A',
	'C',
	'A',
	'B',
	'D',
	'B',
	'B',
	'E',
	'E',
	'F',
	'I',
	'J',
	'H',
	'G',
	'A',
	'A',
	'K',
	'L',
	'M',
	'L',
	'L',
	'M',
]);
$items = $cart->getItems();
$total = $cart->calcTotal();

#endregion

#endregion

#region Test2

$text = "{Пожалуйста|Просто} сделайте так, чтобы это {удивительное|крутое|простое} тестовое предложение {изменялось {быстро|мгновенно} случайным образом|менялось каждый раз}.";
$randomText = (new Regexp())->generateRandomText($text);

#region

?>
<style>
	table {
		border: 1px solid black;
		margin: 10px 5px;
		padding: 5px;
	}

	td, th {
		border: 1px solid black;
		padding: 4px;
	}

	.block {
		border: 1px solid black;
		width: auto;
		padding: 5px;
		margin: 10px 0;
	}

	.dst span {
		background: #9fc1cc;
	}

</style>

<h1>Задание №2</h1>
<div>
	<div class="block src"><?= $text ?></div>
	<div class="block dst"><?= $randomText ?></div>
</div>

<h1>Задание №1</h1>
<table>
	<tr>
		<th>Product</th>
		<th>Retail Price</th>
		<th>Promo Price</th>
		<th>Promo Class</th>
	</tr>
	<? foreach ($items as $item): ?>
		<tr>
			<? /** @var Product $item */ ?>
			<td><?= $item->getName() ?></td>
			<td><?= $item->getPriceText() ?></td>
			<td><?= $item->getPriceText(true) ?></td>
			<td>
				<? if (isset($item->promo)): ?>
					<span><?= $item->promo->discount ?>%</span>
					<span><?= $item->promo->id ?></span>
					<span><?= $item->promo->class ?></span>
				<? endif; ?>
			</td>
		</tr>
	<? endforeach; ?>
</table>

<table>
	<tr>
		<th>Total without promos</th>
		<th>Total with promos</th>
		<th>Promo Class</th>
	</tr>
	<tr>
		<td><?= $total['withoutPromo']; ?></td>
		<td><?= $total['withSummaryPromo']; ?></td>
		<td>
			<? if (isset($cart->summaryDiscount)): ?>
				<span><?= $cart->summaryDiscount->discount ?>%</span>
				<span><?= $cart->summaryDiscount->id ?></span>
				<span><?= $cart->summaryDiscount->class ?></span>
			<? endif; ?>
		</td>
	</tr>
</table>
