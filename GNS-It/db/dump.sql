
CREATE TABLE IF NOT EXISTS `Facts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `dttm_created` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `film_id` (`film_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `FK_Facts_Films` FOREIGN KEY (`film_id`) REFERENCES `Films` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `Films` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` smallint(5) unsigned NOT NULL,
  `isActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `year` (`year`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/* -- DATA -- */

INSERT INTO `Facts` (`id`, `film_id`, `description`, `user_id`, `dttm_created`) VALUES
	(1, 1, 'Норм кинцо', NULL, 1453751046),
	(2, 2, 'Даже не вздумайте смотреть', 101, 1453751514),
	(3, 2, 'Хм...', NULL, 1453751617);

INSERT INTO `Films` (`id`, `name`, `year`, `isActive`) VALUES
	(1, 'Матрица', 1999, b'1'),
	(2, 'Фантасмагория: Видения Льюиса Кэрролла', 2010, b'0'),
	(3, 'Интерстеллар', 2014, b'1');


