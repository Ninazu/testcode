<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Facts */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Add Fact');
$this->params['breadcrumbs'][] = ['label' => $model->getFilmInfo(), 'url' => ['view', 'id'=>$model->film_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facts-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<div class="facts-form">

		<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'film_id')->hiddenInput()->label(false); ?>

		<?= $form->field($model, 'filmInfo')->textInput(['readonly' => true]) ?>

		<?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Add'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>


</div>
