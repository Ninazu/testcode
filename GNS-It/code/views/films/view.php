<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Films */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Films'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="films-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Yii::t('app', 'Add fact'), ['add-fact', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
		<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'method' => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'name',
			'year',
			'isActive:boolean',
		],
	]) ?>

	<ul class="list-group">
		<? foreach ($model->facts as $fact): ?>
			<li class="list-group-item">
				<div class="fact-footer">
					<span class="user"><?= $fact->username ?></span>
					<span class="dttm"><?= $fact->dttm ?></span>
				</div>
				<div><?= $fact->description ?></div>
			</li>
		<? endforeach; ?>
	</ul>
</div>
