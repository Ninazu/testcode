<?php

use app\models\Films;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FilmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Films');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="films-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Yii::t('app', 'Create Films'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php Pjax::begin(); ?>    <?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			'name',
			[
				'attribute' => 'year',
				'filter' => Html::activeDropDownList($searchModel, 'year', [null => ''] + ArrayHelper::map(Films::find()->groupBy('year')->asArray()->all(), 'year', 'year'), ['class' => 'form-control']),
			],
			[
				'attribute' => 'isActive',
				'value' => function ($model) {
					return ($model->isActive) ? \Yii::t('app', 'Active') : \Yii::t('app', 'Inactive');
				},
				'filter' =>
					[
						Films::ACTIVE => \Yii::t('app', 'Active'),
						Films::INACTIVE => \Yii::t('app', 'Inactive'),
					],
			],

			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{facts}{counter}{view}{update}{delete}',
				'buttons' => [
					'view',
					'update',
					'delete',
					'facts' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-plus"></span>',
							Url::toRoute(['add-fact', 'id' => $model->id]),
							[
								'title' => Yii::t('yii', 'Add Facts'),
							]
						);
					}
				],
			],
		],
	]); ?>
	<?php Pjax::end(); ?></div>
