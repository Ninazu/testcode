<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Facts".
 *
 * @property integer $id
 * @property integer $film_id
 * @property string $description
 * @property string $username
 * @property string $dttm
 * @property integer $user_id
 * @property integer $dttm_created
 *
 * @property Films $film
 */
class Facts extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Facts';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['film_id', 'description', 'dttm_created'], 'required'],
			[['film_id', 'user_id', 'dttm_created'], 'integer'],
			[['description'], 'string'],
			[['film_id'], 'exist', 'skipOnError' => true, 'targetClass' => Films::className(), 'targetAttribute' => ['film_id' => 'id']],
		];
	}

	public function beforeValidate() {
		if ($this->isNewRecord) {
			$this->dttm_created = time();
			$this->user_id = Yii::$app->user->id;
		}

		return parent::beforeValidate();
	}

	public function getFilmInfo() {
		if (!isset($this->film)) {
			return \Yii::t('app', 'Film not selected');
		}

		return "{$this->film->name} ({$this->film->year})";
	}

	public function getDttm() {
		if (!$this->dttm_created) {
			return null;
		}

		return \Yii::$app->formatter->asDate($this->dttm_created, 'php:Y-m-d H:i:s');
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'film_id' => Yii::t('app', 'Film ID'),
			'description' => Yii::t('app', 'Description'),
			'user_id' => Yii::t('app', 'User ID'),
			'dttm_created' => Yii::t('app', 'Dttm Created'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFilm() {
		return $this->hasOne(Films::className(), ['id' => 'film_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsername() {
		if (!$this->user_id) {
			return 'guest';
		}

		/** @var User $user */
		if ($user = User::findIdentity($this->user_id)) {
			return $user->username;
		}

		return null;
	}
}
