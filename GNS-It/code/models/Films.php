<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Films".
 *
 * @property integer $id
 * @property string $name
 * @property integer $year
 * @property boolean $isActive
 *
 * @property Facts[] $facts
 */
class Films extends \yii\db\ActiveRecord {

	const ACTIVE = 1;

	const INACTIVE = 0;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Films';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['name', 'year'], 'required'],
			[['year'], 'integer'],
			[['isActive'], 'boolean'],
			[['name'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'ID'),
			'name' => Yii::t('app', 'Name'),
			'year' => Yii::t('app', 'Year'),
			'isActive' => Yii::t('app', 'Is Active'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFacts() {
		return $this->hasMany(Facts::className(), ['film_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 * @return FilmsQuery the active query used by this AR class.
	 */
	public static function find() {
		return new FilmsQuery(get_called_class());
	}
}
